package App;

import java.io.IOException;

import command.Commande;

import TUI.DnsTUI;
import exercices.Dns;

public class DnsApp  {

	
	public static void run () throws IOException{
		
		DnsTUI ui = new DnsTUI();
		Commande cmd= ui.nextCommande();
		ui.Affiche(cmd.execute());
		
	}
	
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		while(true)
			run();
		
	}

}
