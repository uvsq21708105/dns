package command;

import java.io.IOException;

public interface Commande {
  public String execute() throws IOException;

}
