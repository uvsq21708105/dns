package command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import exercices.Dns;
import exercices.DnsItem;
import exercices.NomMachine;

public class TrouverMachineDomain implements Commande{
	
	public String saisie;
	
public TrouverMachineDomain(String saisie) {
		this.saisie = saisie;
	}

public String execute() throws IOException {
		
		Dns dns = new Dns();
		String resultat="";
		List<DnsItem> list = new ArrayList<DnsItem>() ;
		list = dns.getItems(saisie);
		System.out.println(list.size());
		for(DnsItem i : list ){
			
			resultat =i.getNommachine().getNom() +" "+ resultat;
			
		}
		
		return resultat;
		// TODO Auto-generated method stub
		
	}

}
