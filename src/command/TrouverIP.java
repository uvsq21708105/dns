package command;

import java.io.IOException;

import exercices.Dns;
import exercices.DnsItem;
import exercices.NomMachine;

public class TrouverIP implements Commande {

public String saisie;
	
	public TrouverIP(String saisie) {
			
			this.saisie = saisie;
		}

	public String execute() throws IOException {
		
		Dns dns = new Dns();
		NomMachine name = new NomMachine(saisie);
		DnsItem item = dns.getItem(name);
		//System.out.println(item.toString());
		return item.getAdresseIp().getAdress();
		// TODO Auto-generated method stub
		
	}

}
