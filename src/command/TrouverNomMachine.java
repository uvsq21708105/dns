package command;

import java.io.IOException;

import exercices.AdresseIP;
import exercices.Dns;
import exercices.NomMachine;

public class TrouverNomMachine implements Commande {

	public String saisie;
	
	public TrouverNomMachine(String saisie) {
			this.saisie = saisie;
		}
public String execute() throws IOException {
		
		Dns dns = new Dns();
		AdresseIP ip = new AdresseIP(saisie);
		
		return dns.getItem(ip).getNommachine().getNom();
		// TODO Auto-generated method stub
		
	}
	
}
