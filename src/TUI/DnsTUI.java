package TUI;

import java.util.Scanner;

import command.Commande;
import command.Exit;
import command.TrouverIP;
import command.TrouverMachineDomain;
import command.TrouverNomMachine;

public class DnsTUI {

	public Commande nextCommande (){
		Scanner scanner = new Scanner(System.in) ;
        String x = scanner.next();
        
        if(!x.contains(".") && !x.contains("exit")){
        	
        	return new TrouverMachineDomain(x);
        }else if(x.contains("1")){
        	
        	return new TrouverNomMachine(x);
        }else if(x.contains("exit")){
        	
        	return new Exit();
        }else {
        	
        	return new TrouverIP(x);
        }
		
	}
	
	public Commande nextCommande (String s){
		
        String x = s;
        
        if(!x.contains(".") && !x.contains("exit")){
        	
        	return new TrouverMachineDomain(x);
        }else if(x.contains("1")){
        	
        	return new TrouverNomMachine(x);
        }else if(x.contains("exit")){
        	
        	return new Exit();
        }else {
        	
        	return new TrouverIP(x);
        }
		
	}
	public void Affiche (String resultat){
		
		System.out.println(resultat);
		
	}
	
	
	
	
	
	
	
}

