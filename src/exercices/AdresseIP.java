package exercices;

public class AdresseIP {
	
	
	private String adress;

	public AdresseIP(String adress) {
		super();
		this.adress = adress;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}
	

}
