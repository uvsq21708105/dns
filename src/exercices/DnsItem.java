package exercices;

public class DnsItem {

	private AdresseIP adresseIp;
	private NomMachine nommachine;
	public DnsItem(AdresseIP adresseIp, NomMachine nommachine) {
		super();
		this.adresseIp = adresseIp;
		this.nommachine = nommachine;
	}
	public AdresseIP getAdresseIp() {
		return adresseIp;
	}
	public void setAdresseIp(AdresseIP adresseIp) {
		this.adresseIp = adresseIp;
	}
	public NomMachine getNommachine() {
		return nommachine;
	}
	public void setNommachine(NomMachine nommachine) {
		this.nommachine = nommachine;
	}
	public DnsItem() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
