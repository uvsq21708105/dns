package exercices;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Properties;

import javax.naming.spi.DirStateFactory.Result;


public class Dns {

	Properties defaultProps;
	List<DnsItem> dnsCollection = new ArrayList<DnsItem>();
	
	public Dns() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	
		
			
			Properties defaultProps = new Properties();
			defaultProps.put("db", "dnsDatabase");
			FileReader in = new FileReader(defaultProps.getProperty("db"));

			int content;
			String cle="";
			String valeur="";
			DnsItem dnsItem1 = new DnsItem(null, null);
			NomMachine key = null;
			AdresseIP nom = null;
			while ((content = in.read()) != -1) {
				// convert to char and display it
				switch (content){
				 case 13: break;
				 case 32: dnsItem1 = new DnsItem(null, null);key= new NomMachine(cle); dnsItem1.setNommachine(key) ;cle="fini"; break;
				 case 10: nom= new AdresseIP(valeur) ;dnsItem1.setAdresseIp(nom);
				 			dnsCollection.add(dnsItem1);valeur="";cle="";key=null;nom=null; break;
				 default: if(cle.equals("fini")){
					 valeur=valeur+(char)content;
				 }
				 else 
					
					 cle=cle+(char)content;
				}
					
				
			}
	
			//System.out.println(dnsCollection.get(0).getNommachine().getNom());
	}
	public DnsItem getItem (NomMachine nom)
	{
		DnsItem n= new DnsItem();
		for(DnsItem a : dnsCollection){
			if(a.getNommachine().getNom().equals(nom.getNom()))
				n=a;
		}
		return n;
		
	}
	public DnsItem getItem (AdresseIP ip)
	{
		DnsItem n= new DnsItem();
		for(DnsItem a : dnsCollection){
			if(a.getAdresseIp().getAdress().equals(ip.getAdress()))
				n=a;
		}
		return n;
		
	}
	public List<DnsItem> getItems (String domain)
	{
		List<DnsItem> resultat = new ArrayList<DnsItem>() ;

		for(DnsItem a : dnsCollection){
			if(a.getNommachine().getNom().contains(domain))
				resultat.add(a) ;
		}
		
		return resultat;
		
	}
	
}
